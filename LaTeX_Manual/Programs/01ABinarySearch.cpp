#include <iostream>
using namespace std;

class BinarySearch
{
	int iaArr[50], iNum, iKey, iPos, iLow, iHigh;
	
	public:
	BinarySearch(){iPos = -1; iLow = 0;}
	void fnSearchKey();
	friend ostream& operator<<(ostream&, const BinarySearch&);
	friend istream& operator>>(istream&, BinarySearch&);
};

void BinarySearch ::fnSearchKey()
{
	if(iLow<=iHigh)
	{
		int iMid = (iLow + iHigh)/2;
		if(iKey == iaArr[iMid])
			iPos = iMid;
		else if(iKey < iaArr[iMid])
		{
			iHigh = iMid - 1;
			fnSearchKey();
		}		
		else 
		{
			iHigh = iMid - 1;
			fnSearchKey();
		}
	}
}

ostream& operator<<(ostream &out, const BinarySearch &b)
{
	if(b.iPos != -1)
	{
		out << "In the array : " <<endl;
		for(int i=0;i<b.iNum;i++)
			out << b.iaArr[i] << "\t";
		out << endl;
		out << "The key " << b.iKey << " is found at position " << b.iPos+1 << " in the array." << endl;	
	}
	else
	{
		out << "The key " << b.iKey << " is not found. " <<	endl;
	}
	return out;
}

istream& operator>>(istream &in, BinarySearch &b)
{
	cout << "Enter the number of elements : ";
	in >> b.iNum;
	b.iHigh = b.iNum -1;
	cout << "Enter " << b.iNum << " elements in ascending order : " << endl;	
	for(int i=0;i<b.iNum;i++)
			in >> b.iaArr[i];
	cout << "Enter the key element" << endl;
	in >> b.iKey;
	return in;	
}

int main()
{
	BinarySearch Search1;
	cin >> Search1;
	Search1.fnSearchKey();
	cout << Search1;
	return 0;
}
