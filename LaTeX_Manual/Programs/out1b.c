/***************************************
OUTPUT
SAMPLE 1
Enter the size of the array
6
Enter the elements of the array:
7 2 3 6 5 4
Max Element = 7
Min Element = 2
SAMPLE 2
Enter the size of the array
7
Enter the elements of the array:
7 1 2 6 5 3 4
Max Element = 7
Min Element = 1
SAMPLE 3
Enter the size of the array
7
Enter the elements of the array:
1 3 5 7 2 4 6
Max Element = 7
Min Element = 1
******************************************************************************/
