/***************************************
Sample 1
Enter the number of vertices : 5
Enter the Cost Adjacency Matrix
0		3		9999	7		9999
3		0		4		2		9999
9999	4		0		5		6
7		2		5		0		4
9999	9999	6		4		0
The spanning tree exists and minimum cost spanning tree is 
3 1
1 0
3 4
1 2
The cost of the minimum cost spanning tree is 13

Sample 2
Enter the number of vertices : 5
Enter the Cost Adjacency Matrix
0		3		9999	7		9999
3		0		9999	2		9999
9999	9999	0		9999	9999
7		2		9999	0		4	
9999	9999	9999	4		0

Spanning tree does not exist

***************************************/

