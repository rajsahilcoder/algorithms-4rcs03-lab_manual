/***************************************
Enter the maximum number of elements : 5
Enter the weights of the elements : 
1 2 3 5 6 
Enter the total required weight : 9

SOLUTION #1 IS
{ 1 2 6 }

SOLUTION #2 IS
{ 1 3 5 }

SOLUTION #3 IS
{ 3 6 }

The above-mentioned sets are the required solution to the given instance.

Enter the maximum number of elements : 4
Enter the weights of the elements : 
1 3 5 9
Enter the total required weight : 7

The given problem instance doesnt have any solution.

***************************************/
