/******************************************************************************
*File			: 04BInsertion.c
*Description	: Program to sort an array using Insertion Sort
*Author			: Prabodh C P
*Compiler		: gcc compiler 7.5.0, Ubuntu 18.04
*Date			: Friday 4 February 2020
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

void fnGenRandInput(int [], int);
void fnDispArray( int [], int);
void fnInsertionSort(int [], int);

/******************************************************************************
*Function	: main
*Function parameters:
*	int argc - no of commamd line arguments
*	char **argv - vector to store command line argumennts
*RETURNS	:	0 on success
******************************************************************************/
int main( int argc, char **argv)
{
	FILE *fp;
	struct timeval tv;
	double dStart,dEnd;
	int iaArr[500000],iNum,i,iChoice;
    for(;;)
    {
        printf("\n1.Plot the Graph\n2.Insertion Sort\n3.Exit");
        printf("\nEnter your choice\n");
        scanf("%d",&iChoice);
        switch(iChoice)
        {
            case 1:
                fp = fopen("InsertPlot.dat","w");

                for(i=100;i<10000;i+=100)
                {
                    fnGenRandInput(iaArr,i);
                    gettimeofday(&tv,NULL);
                    dStart = tv.tv_sec + (tv.tv_usec/1000000.0);
					fnInsertionSort(iaArr,i);
                    gettimeofday(&tv,NULL);
                    dEnd = tv.tv_sec + (tv.tv_usec/1000000.0);
                    fprintf(fp,"%d\t%lf\n",i,dEnd-dStart);
                }
                fclose(fp);
                printf("\nData File generated and stored in file < InsertPlot.dat >.\n Use a plotting utility\n");
            break;

            case 2:
                printf("\nEnter the number of elements to sort\n");
                scanf("%d",&iNum);
                printf("\nUnsorted Array\n");
                fnGenRandInput(iaArr,iNum);
                fnDispArray(iaArr,iNum);
                fnInsertionSort(iaArr,iNum);
                printf("\nSorted Array\n");
                fnDispArray(iaArr,iNum);
            break;

            case 3:	exit(0);
        }
    }
	return 0;
}

/******************************************************************************
*Function	: fnInsertionSort
*Description	: Function to sort elements in an iaArray using Insertion Sort
*Function parameters:
*	int A[] - iaArray to hold integers
*	int n - no of elements in the array
*RETURNS	: no value
******************************************************************************/
void fnInsertionSort(int A[],int n)
{
	int i, j, iKey;
	for(i=1;i<n;i++)
	{
		iKey = A[i];
		j = i-1;
		while(j>=0 && A[j] > iKey)
		{
			A[j+1] = A[j];
			j--;
		}
		A[j+1] = iKey;
	}
}

/******************************************************************************
*Function	: GenRandInput
*Description	: Function to generate a fixed number of random elements
*Function parameters:
*	int X[] - array to hold integers
*	int n	- no of elements in the array
*RETURNS	:no return value
******************************************************************************/
void fnGenRandInput(int X[], int n)
{
	srand(time(NULL));
	for(int i=0;i<n;i++)
	{
		X[i] = rand()%10000;
	}
}

/******************************************************************************
*Function	: DispArray
*Description	: Function to display elements of an array
*Function parameters:
*	int X[] - array to hold integers
*	int n	- no of elements in the array
*RETURNS	: no return value
******************************************************************************/
void fnDispArray( int X[], int n)
{
	for(int i=0;i<n;i++)
		printf(" %5d \n",X[i]);

}
