/******************************************************************************
*File		: 05BBFS.cpp
*Description: Program to find all nodes reachable from a given node using BFS
*Author		: Prabodh C P
*Compiler	: g++ compiler 7.5.0, Ubuntu 18.04
*Date		: 31 Mar 2020
******************************************************************************/
#include <iostream>
#include <queue> 
using namespace std;
const int MAX = 100;
void fnBreadthFirstSearchReach(int vertex, int g[MAX][MAX], int v[MAX], int n);

/******************************************************************************
*Function	: main
*Input parameters: no parameters
*RETURNS	: 0 on success
******************************************************************************/
int main(void)
{
	int graph[MAX][MAX];
	int visited[MAX];
	int numVert, startVert, i,j;
	cout << "Enter the number of vertices : ";
	cin >> numVert;
	cout << "Enter the adjacency matrix :\n";
	for (i=0; i<numVert; i++)
		visited[i] = 0;
	for (i=0; i<numVert; i++)
		for (j=0; j<numVert; j++)
			cin >> graph[i][j];
	cout << "Enter the starting vertex : ";
	cin >> startVert;
	fnBreadthFirstSearchReach(startVert-1,graph,visited,numVert);
	cout << "Vertices which can be reached from vertex " << startVert << " are :-" << endl;
	for (i=0; i<numVert; i++)
		if (visited[i])
			cout << i+1 << " ";
	cout << endl;
	return 0;
}

/******************************************************************************	
*Function	: fnBreadthFirstSearchReach
*Description	: Function to perform BFS traversal and mark visited vertices
*Input parameters:
*	int vertex - source vertex
*	int g[][]	- adjacency matrix of the graph
*	int v[]	- vector to store visited information
*	int n	- no of vertices
*RETURNS	: void
******************************************************************************/
void fnBreadthFirstSearchReach(int vertex, int g[MAX][MAX], int v[MAX], int n)
{
	queue <int> VQueue;
	int frontVertex, i;
	v[vertex] = 1;
	VQueue.push(vertex);
	while (!VQueue.empty())
	{
		frontVertex = VQueue.front();
		VQueue.pop();
		for (i=0; i<n; i++)
		{
			if (g[frontVertex][i] && !v[i])
			{
				v[i] = 1;
				VQueue.push(i);
			}
		}
	}
}
