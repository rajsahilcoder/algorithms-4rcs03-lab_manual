/***************************************
Enter the number of nodes : 6
Enter the number of edges : 10
Enter edge and cost in the form u, v, w :	0 1 6
Enter edge and cost in the form u, v, w :	1 4 3
Enter edge and cost in the form u, v, w :	4 5 6
Enter edge and cost in the form u, v, w :	5 3 2
Enter edge and cost in the form u, v, w :	3 0 5
Enter edge and cost in the form u, v, w :	0 2 1
Enter edge and cost in the form u, v, w :	1 2 5
Enter edge and cost in the form u, v, w :	3 2 5
Enter edge and cost in the form u, v, w :	4 2 6
Enter edge and cost in the form u, v, w :	5 2 4

Spanning tree exists
The Spanning tree is shown below
0 2
5 3
1 4
5 2
1 2

Cost of the spanning tree : 15

***************************************/

